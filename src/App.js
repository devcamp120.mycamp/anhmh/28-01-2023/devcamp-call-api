import "bootstrap/dist/css/bootstrap.min.css";
import FetchAPI from "./components/FetchAPI";

function App() {
  return (
    <div className="container mt-5">
      <FetchAPI/>
    </div>
  );
}

export default App;
